<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class UpdateFCMRegID {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function updateUserFCMRegID($username, $phonenumber) {
		try {
			
			$select_query = "SELECT username, phone_number FROM users WHERE username = '$username' AND phone_number = '$phonenumber';";
			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) > 0) {
				$update_query = "UPDATE users SET fcm_reg_id = 'logged out' WHERE username = '$username' AND phone_number = '$phonenumber';";
				$update_result = mysqli_query($this->connection, $update_query);

				if ($update_result == 1) {
						$json['success'] = "Updated successfully!";
					} else {
						$json['error'] = "Error while updating, please try again!";
					}	

				echo json_encode($json);
				mysqli_close($this->connection);

			}

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
}


$updateFCMRegID = new UpdateFCMRegID();
if(isset($_POST['username'], $_POST['phonenumber'])) {

	$username = $_POST['username'];
	$phonenumber = $_POST['phonenumber'];

	if(!empty($username) && !empty($phonenumber)) {
			$updateFCMRegID->updateUserFCMRegID($username, $phonenumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class UpdateMessageStatus {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function updateTheMessageStatus($sender_phone_number, $receiver_phone_number, $question, $sender_fcm_token, $recipient_fcm_token, $confirmed_option, $name_of_confirmer) {

		try {
			
			$update_query = "UPDATE friend_message SET message_status = '1', option_picked = '$confirmed_option' WHERE user_one_phone = '$sender_phone_number' AND user_two_phone = '$receiver_phone_number' AND question = '$question' AND message_status = '0';";

			$update_result = mysqli_query($this->connection, $update_query);

			if ($update_result == 1) {
				$json['success'] = 'Updated successfully!';
			} else {
				$json['error'] = 'Problem while updating!';
			}

			echo json_encode($json);
			mysqli_close($this->connection);

			/*
				Implementing the Firebase Cloud Messaging Service
			*/
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array(
						'to' => $recipient_fcm_token, 
						'notification' => array("body" => $confirmed_option),
						'data' => array("confirmed_option" => $confirmed_option,
										"receiver_name" => $name_of_confirmer,
										"receiver_phone_number" => $receiver_phone_number)
				);

			//Authorization key value is the "Firebase Cloud Messaging token"
			$headers = array(
				'Authorization:key=AAAAvb8pTks:APA91bEkrZg468ZfyK_oEsiYqX5O_tydA0Q8xoude-i29qx5D7Zsc4ESZxotQCTCQOf0zBV9QNErQrE6mEBHxNsSC_uvN1UgRE7juBEDYOMa4I4DPjTXyv9gsmArrQiaB2pmiK-bNXG0CUkWfrZpyPmzTjuEuENjgA',
				'Content-Type: application/json'
			);
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
			curl_close($ch);
			

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$updateMessageStatus = new UpdateMessageStatus();
if(isset($_POST['sender_phone_number'], $_POST['receiver_phone_number'], $_POST['question'])) {

	$sender_phone_number = $_POST['sender_phone_number'];
	$receiver_phone_number = $_POST['receiver_phone_number'];
	$question = $_POST['question'];
	$sender_fcm_token = $_POST['sender_fcm_token'];
	$recipient_fcm_token = $_POST['recipient_fcm_token'];
	$confirmed_option = $_POST['confirmed_option'];
	$name_of_confirmer = $_POST['name_of_confirmer'];

	if (!empty($sender_phone_number) && !empty($receiver_phone_number) && !empty($question)) {
		$updateMessageStatus->updateTheMessageStatus($sender_phone_number, $receiver_phone_number, $question, $sender_fcm_token, $recipient_fcm_token, $confirmed_option, $name_of_confirmer);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
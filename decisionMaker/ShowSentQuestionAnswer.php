<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class ShowSentQuestionAnswer {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getSentQuestionAnswer($sender_phoneNumber, $receiver_phoneNumber, $question) {

		try {
			
				$select_query = "SELECT question, option_one, option_two, option_three, option_four, option_picked FROM friend_message WHERE user_one_phone = '$sender_phoneNumber' AND user_two_phone = '$receiver_phoneNumber' AND question = '$question' AND message_status = '1';";
				$result_query = mysqli_query($this->connection, $select_query);

				if (mysqli_num_rows($result_query) > 0) {
					$json = array();
					while ($row = mysqli_fetch_assoc($result_query)) {
						array_push($json, $row);
					}
					echo json_encode($json);
				} else {
					$json['no_data'] = 'Question has not been answered yet!';
					echo json_encode($json);
				}
				mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$showSentQuestionAnswer = new ShowSentQuestionAnswer();
if(isset($_GET['sender_phoneNumber'], $_GET['receiver_phoneNumber'], $_GET['question'])) {

	$sender_phoneNumber = $_GET['sender_phoneNumber'];
	$receiver_phoneNumber = $_GET['receiver_phoneNumber'];
	$question = $_GET['question'];

	if (!empty($sender_phoneNumber) && !empty($receiver_phoneNumber) && !empty($question)) {
		$showSentQuestionAnswer->getSentQuestionAnswer($sender_phoneNumber, $receiver_phoneNumber, $question);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
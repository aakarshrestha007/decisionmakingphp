<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class UnDecidedMessages {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function retriveUndecidedMessages($receiver_name) {

		try {
			
			$select_query = "SELECT distinct user_one_phone, user_two_phone, sender_name, receiver_name, message_status, createdDate FROM friend_message WHERE user_two_phone = '$receiver_name' AND message_status = '0' ORDER BY createdDate DESC;";
			$result_query = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($result_query) > 0) {
				$json = array();
				while ($row = mysqli_fetch_assoc($result_query)) {
					array_push($json, $row);
				}
				echo json_encode($json);
			} else {
				$json['error'] = 'you have no friends in the list!';
				echo json_encode($json);
			}

			mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$unDecidedMessages = new UnDecidedMessages();
if(isset($_GET['receiver_phone_number'])) {

	$receiver_name = $_GET['receiver_phone_number'];

	if (!empty($receiver_name)) {
		$unDecidedMessages->retriveUndecidedMessages($receiver_name);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
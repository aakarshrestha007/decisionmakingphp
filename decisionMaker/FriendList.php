<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class FriendList {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getFriendList($phonenumber) {

		try {
			
			$select_query = "SELECT user_one_name, user_two_name, user_two_phone FROM friend_hash WHERE user_one_phone = '$phonenumber' OR user_two_phone = '$phonenumber';";

			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) > 0) {
				$json = array();

				while ($row = mysqli_fetch_assoc($select_result)) {
					array_push($json, $row);
				}

				echo json_encode($json);

			} else {
				$json['error'] = 'no friend!';
				echo json_encode($json);
			}

			mysqli_close($this->connection);


		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$friendList = new FriendList();
if(isset($_GET['phonenumber'])) {

	$phonenumber = $_GET['phonenumber'];

	if (!empty($phonenumber)) {
		$friendList->getFriendList($phonenumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
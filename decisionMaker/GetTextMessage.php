<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class GetTextMessage {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getTextMessages($sender_phone, $receiver_phone, $limitNumber=0) {
		
		try {
			$num = 10;
			$limit_num = $num+$limitNumber;
			//get the hashID from friend_hash table
			$select_query = "SELECT hashID FROM friend_hash WHERE (user_one_phone = '$sender_phone' AND user_two_phone = '$receiver_phone') OR (user_one_phone = '$receiver_phone' AND user_two_phone = '$sender_phone');";
			$select_result = mysqli_query($this->connection, $select_query);
			if (mysqli_num_rows($select_result) == 1) {
				while ($hash_value = mysqli_fetch_array($select_result)) {
					$h_value = $hash_value['hashID'];
				}

				// get the total count of the message
				$getcount_select_query = "SELECT count(*) AS total, sender_phone, receiver_phone, message FROM text_message WHERE hashID = '$h_value';";
				$getcount_result_query = mysqli_query($this->connection, $getcount_select_query);
				$row = mysqli_fetch_assoc($getcount_result_query);
				$totalCount = $row['total'];

				if ($totalCount > $limit_num) {
					$query_to_select = "SELECT sender_phone, receiver_phone, message FROM text_message WHERE hashID = '$h_value' ORDER BY createdDate DESC LIMIT $limit_num OFFSET 0;";
					$query_result = mysqli_query($this->connection, $query_to_select);

					if (mysqli_num_rows($query_result) > 0) {
						$json = array();
						while ($row = mysqli_fetch_assoc($query_result)) {
							array_push($json, $row);
						}

						echo json_encode($json);
					}
				} else {
					$query_to_select = "SELECT sender_phone, receiver_phone, message FROM text_message WHERE hashID = '$h_value' ORDER BY createdDate DESC LIMIT $limit_num OFFSET 0;";
					$query_result = mysqli_query($this->connection, $query_to_select);

					if (mysqli_num_rows($query_result) > 0) {
						$json = array();
						while ($row = mysqli_fetch_assoc($query_result)) {
							array_push($json, $row);
						}

						echo json_encode($json);
					}
				}

				
			}	

			mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
		
	}
	
}


$getTextMessage = new GetTextMessage();

if (isset($_GET['sender_phone'], $_GET['receiver_phone'], $_GET['limitNumber'])) {
	$sender_phone = $_GET['sender_phone'];
	$receiver_phone = $_GET['receiver_phone'];
	$message = $_GET['message'];
	$limitNumber = $_GET['limitNumber'];

	if (!empty($sender_phone) && !empty($receiver_phone)) {
		$getTextMessage->getTextMessages($sender_phone, $receiver_phone, $limitNumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class FriendConnection {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function connectWithFriend($user_one_phone, $user_two_phone, $sender_name, $receiver_name, $message_status, $question, $option_one, $option_two, $option_three, $option_four, $sender_fcm_token) {

		try {
			/*
				Get the user_two fcm id from users table
			*/
			$select_query_to_get_fcm_reg_id = "SELECT fcm_reg_id FROM users WHERE phone_number = '$user_two_phone';";
			$select_result_to_get_fcm_reg_id = mysqli_query($this->connection, $select_query_to_get_fcm_reg_id);

			if (mysqli_num_rows($select_result_to_get_fcm_reg_id) == 1) {
				while ($user_two_fcm_reg_id = mysqli_fetch_array($select_result_to_get_fcm_reg_id)) {
					$userTwo_fcm_id = $user_two_fcm_reg_id['fcm_reg_id'];
				}
			}

			
			$hash_value = rand();
			
			$select_query = "SELECT hashID FROM friend_hash WHERE (user_one_phone = '$user_one_phone' AND user_two_phone = '$user_two_phone') OR (user_one_phone = '$user_two_phone' AND user_two_phone = '$user_one_phone');";
			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) == 1) {
				
				while ($hash_value = mysqli_fetch_array($select_result)) {
					$h_value = $hash_value['hashID'];
				}

				$insert_query = "INSERT INTO friend_message (id, hashID, user_one_phone, user_two_phone, sender_name, receiver_name, message_status, question, option_one, option_two, option_three, option_four, sender_fcm_token, recipient_fcm_token) VALUES ('', '$h_value', '$user_one_phone', '$user_two_phone', '$sender_name', '$receiver_name', $message_status, '$question', '$option_one', '$option_two', '$option_three', '$option_four', '$sender_fcm_token', '$userTwo_fcm_id');";

				$insert_result = mysqli_query($this->connection, $insert_query);

				if ($insert_result == 1) {
					$json['success'] = 'Message sent successfully!';
				} else {
					$json['error'] = 'Error sending message, please try again!';
				}

				echo json_encode($json);
				mysqli_close($this->connection);

				/*
					Implementing Cloud Messaging
				*/

				$url = 'https://fcm.googleapis.com/fcm/send';
				$fields = array(
						'to' => $userTwo_fcm_id,
						'notification' => array("title" => "New message","body" => $sender_name . ' is asking: ' . $question),
						'data' => array("message" => $question,
										"senderName" => $sender_name,
										"senderPhone" => $user_one_phone,
										"recipient_fcm_token" => $sender_fcm_token)
					);

				//Authorization key value is the "Firebase Cloud Messaging token"
				$headers = array(
						'Authorization:key=AAAAvb8pTks:APA91bEkrZg468ZfyK_oEsiYqX5O_tydA0Q8xoude-i29qx5D7Zsc4ESZxotQCTCQOf0zBV9QNErQrE6mEBHxNsSC_uvN1UgRE7juBEDYOMa4I4DPjTXyv9gsmArrQiaB2pmiK-bNXG0CUkWfrZpyPmzTjuEuENjgA',
						'Content-Type: application/json'
					);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				$result = curl_exec($ch);
				if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}
				curl_close($ch);

			} else {

				$query = "INSERT INTO friend_hash (user_one_phone, user_two_phone, user_one_name, user_two_name, hashID) VALUES ('$user_one_phone', '$user_two_phone', '$sender_name', '$receiver_name', '$hash_value');";

				$insert_query = "INSERT INTO friend_message (id, hashID, user_one_phone, user_two_phone, sender_name, receiver_name, message_status, question, option_one, option_two, option_three, option_four, sender_fcm_token, recipient_fcm_token) VALUES ('', '$hash_value', '$user_one_phone', '$user_two_phone', '$sender_name', '$receiver_name', $message_status, '$question', '$option_one', '$option_two', '$option_three', '$option_four', '$sender_fcm_token', '$userTwo_fcm_id');";
				
				$result = mysqli_query($this->connection, $query);

				if ($result == 1) {
					$json['success'] = "Connected to your friend! Message sent successfully!";
				} else {
					$json['error'] = "Connection failed!";
				}

				$insert_result = mysqli_query($this->connection, $insert_query);


				echo json_encode($json);
				mysqli_close($this->connection);

				/*
					Implementing Cloud Messaging
				*/

				$url = 'https://fcm.googleapis.com/fcm/send';
				$fields = array(
						'to' => $userTwo_fcm_id,
						'notification' => array("body" => $question),
						'data' => array("message" => $question,
						"recipient_fcm_token" => $sender_fcm_token)
					);

				$headers = array(
						'Authorization:key=AAAAvb8pTks:APA91bEkrZg468ZfyK_oEsiYqX5O_tydA0Q8xoude-i29qx5D7Zsc4ESZxotQCTCQOf0zBV9QNErQrE6mEBHxNsSC_uvN1UgRE7juBEDYOMa4I4DPjTXyv9gsmArrQiaB2pmiK-bNXG0CUkWfrZpyPmzTjuEuENjgA',
						'Content-Type: application/json'
					);
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				$result = curl_exec($ch);
				if ($result === FALSE) {
					die('Curl failed: ' . curl_error($ch));
				}
				curl_close($ch);

			}
			

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$friendConnection = new FriendConnection();
if(isset($_POST['user_one_phone'], $_POST['user_two_phone'])) {

	$user_one_phone = $_POST['user_one_phone'];
	$user_two_phone = $_POST['user_two_phone'];
	$sender_name = $_POST['sender_name'];
	$receiver_name = $_POST['receiver_name'];
	$message_status = $_POST['message_status'];
	$question = $_POST['question'];
	$option_one = $_POST['option_one'];
	$option_two = $_POST['option_two'];
	$option_three = $_POST['option_three'];
	$option_four = $_POST['option_four'];
	$sender_fcm_token = $_POST['sender_fcm_token'];

	if (!empty($user_one_phone) && !empty($user_two_phone)) {
		$friendConnection->connectWithFriend($user_one_phone, $user_two_phone, $sender_name, $receiver_name, $message_status, $question, $option_one, $option_two, $option_three, $option_four, $sender_fcm_token);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
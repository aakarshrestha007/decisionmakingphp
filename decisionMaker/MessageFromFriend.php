<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class MessageFromFriend {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getMessageFromFriend($sender_phonenumber, $receiver_phonenumber) {

		try {
			
			$select_query = "SELECT user_one_phone, user_two_phone, sender_name, receiver_name, message_status, question, option_one, option_two, option_three, option_four, createdDate FROM friend_message WHERE user_one_phone = '$sender_phonenumber' AND user_two_phone = '$receiver_phonenumber' AND message_status = '0' ORDER BY createdDate DESC LIMIT 1;";

			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) > 0) {
				$json = array();

				while ($row = mysqli_fetch_assoc($select_result)) {
					array_push($json, $row);
				}

				echo json_encode($json);

			} else {
				$json['error'] = 'error';
				echo json_encode($json);
			}

			mysqli_close($this->connection);


		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$messageFromFriend = new MessageFromFriend();
if(isset($_GET['sender_phonenumber'], $_GET['receiver_phonenumber'])) {

	$sender_phonenumber = $_GET['sender_phonenumber'];
	$receiver_phonenumber = $_GET['receiver_phonenumber'];

	if (!empty($sender_phonenumber) && !empty($receiver_phonenumber)) {
		$messageFromFriend->getMessageFromFriend($sender_phonenumber, $receiver_phonenumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
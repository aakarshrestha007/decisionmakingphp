<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class FriendExistence {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getFriendExistence($phonenumber) {

		try {
			
			$select_query = "SELECT phone_number, fcm_reg_id FROM users WHERE phone_number = '$phonenumber';";
			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) > 0) {

				$select_query_one = "SELECT fcm_reg_id FROM users WHERE phone_number = '$phonenumber';";
				$select_result_one = mysqli_query($this->connection, $select_query_one);

				while ($user_fcm_token = mysqli_fetch_array($select_result_one)) {
					$user_two_fcm_token = $user_fcm_token['fcm_reg_id'];
				}

				$json['friendExistStatus'] = $phonenumber;
				$json['user_two_fcm_token'] = $user_two_fcm_token;
			} else {
				$json['friendExistStatus'] = "false";
			}

			echo json_encode($json);
			mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$friendExistence = new FriendExistence();
if(isset($_GET['phonenumber'])) {

	$phonenumber = $_GET['phonenumber'];

	if (!empty($phonenumber)) {
		$friendExistence->getFriendExistence($phonenumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
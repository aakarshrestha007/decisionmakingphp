<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class RetriveFriendReceiver {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function retriveFriendList($user_phone) {

		try {
			
			$select_query = "SELECT user_one_name, user_one_phone, user_two_name, user_two_phone, user_one_fcm_token, user_two_fcm_token FROM friend_list WHERE user_two_phone = '$user_phone';";
			$result_query = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($result_query) > 0) {
				$json = array();
				while ($row = mysqli_fetch_assoc($result_query)) {
					array_push($json, $row);
				}

				echo json_encode($json);
			} else {
				$json['error'] = 'you have no friends in the list!';
				echo json_encode($json);
			}

			mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$retriveFriendReceiver = new RetriveFriendReceiver();
if(isset($_GET['user_phone'])) {

	$user_phone = $_GET['user_phone'];

	if (!empty($user_phone)) {
		$retriveFriendReceiver->retriveFriendList($user_phone);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class MessageListInProfileOne {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getMessageListInProfileOne($sender_phoneNumber) {

		try {
			
			$select_query = "SELECT user_one_phone, sender_name, user_two_phone, receiver_name, question, option_picked, createdDate FROM friend_message WHERE user_two_phone = '$sender_phoneNumber' ORDER BY createdDate DESC;";

			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) > 0) {
				$json = array();

				while ($row = mysqli_fetch_assoc($select_result)) {
					array_push($json, $row);
				}

				echo json_encode($json);
			} else {
				$json['error'] = 'error';
				echo json_encode($json);
			}

			mysqli_close($this->connection);


		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$messageListInProfileone = new MessageListInProfileOne();

if (isset($_GET['sender_phoneNumber'])) {

	$sender_phoneNumber = $_GET['sender_phoneNumber'];

	if (!empty($sender_phoneNumber)) {
		$messageListInProfileone->getMessageListInProfileOne($sender_phoneNumber);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
	
}

?>
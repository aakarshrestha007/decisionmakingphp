<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class TextMessageSend {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function sendTextMsg($sender_phone, $receiver_phone, $username_sender, $username_receiver, $message, $sender_fcm_token) {
		
		try {
			/*
				Get the user_two fcm id from users table
			*/
			$select_query_to_get_fcm_reg_id = "SELECT fcm_reg_id FROM users WHERE phone_number = '$receiver_phone';";
			$select_result_to_get_fcm_reg_id = mysqli_query($this->connection, $select_query_to_get_fcm_reg_id);

			if (mysqli_num_rows($select_result_to_get_fcm_reg_id) == 1) {
				while ($receiver_fcm_reg_id = mysqli_fetch_array($select_result_to_get_fcm_reg_id)) {
					$receiver_fcm_id = $receiver_fcm_reg_id['fcm_reg_id'];
				}
			}

			//get the hashID from friend_hash table
			$select_query = "SELECT hashID FROM friend_hash WHERE (user_one_phone = '$sender_phone' AND user_two_phone = '$receiver_phone') OR (user_one_phone = '$receiver_phone' AND user_two_phone = '$sender_phone');";
			$select_result = mysqli_query($this->connection, $select_query);

			if (mysqli_num_rows($select_result) == 1) {
				while ($hash_value = mysqli_fetch_array($select_result)) {
					$h_value = $hash_value['hashID'];
				}

				$insert_query = "INSERT INTO text_message (id, hashID, username_sender, username_receiver, sender_phone, receiver_phone, message, sender_fcm_token, receiver_fcm_token) VALUES ('', '$h_value', '$username_sender', '$username_receiver', '$sender_phone', '$receiver_phone', '$message', '$sender_fcm_token', '$receiver_fcm_id');";

				$insert_result = mysqli_query($this->connection, $insert_query);
				if ($insert_result == 1) {
					$json['success'] = 'Text message sent successfully!';

					/*
					Implementing Cloud Messaging
					*/

					$url = 'https://fcm.googleapis.com/fcm/send';
					$fields = array(
							'to' => $receiver_fcm_id,
							'notification' => array("title" => "New text message","body" => $username_sender . ' says: ' . $message),
							'data' => array("textmessage" => $message,
											"textsenderName" => $username_sender,
											"textsenderPhone" => $sender_phone,
											"recipient_fcm_token" => $sender_fcm_token)
						);

					//Authorization key value is the "Firebase Cloud Messaging token"
					$headers = array(
							'Authorization:key=AAAAvb8pTks:APA91bEkrZg468ZfyK_oEsiYqX5O_tydA0Q8xoude-i29qx5D7Zsc4ESZxotQCTCQOf0zBV9QNErQrE6mEBHxNsSC_uvN1UgRE7juBEDYOMa4I4DPjTXyv9gsmArrQiaB2pmiK-bNXG0CUkWfrZpyPmzTjuEuENjgA',
							'Content-Type: application/json'
						);
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

					$result = curl_exec($ch);
					if ($result === FALSE) {
						die('Curl failed: ' . curl_error($ch));
					}
					curl_close($ch);

				} else {
					$json['error'] = 'Error sending message, please try again!';
				}

				echo json_encode($json);
				mysqli_close($this->connection);

			}

			
		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
}


$textMessageSend = new TextMessageSend();

if (isset($_POST['sender_phone'], $_POST['receiver_phone'])) {
	$sender_phone = $_POST['sender_phone'];
	$receiver_phone = $_POST['receiver_phone'];
	$username_sender = $_POST['username_sender'];
	$username_receiver = $_POST['username_receiver'];
	$message = $_POST['message'];
	$sender_fcm_token = $_POST['sender_fcm_token'];

	if (!empty($sender_phone) && !empty($receiver_phone) && !empty($username_sender) && !empty($username_receiver) && !empty($message) && !empty($sender_fcm_token)) {
		$textMessageSend->sendTextMsg($sender_phone, $receiver_phone, $username_sender, $username_receiver, $message, $sender_fcm_token);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
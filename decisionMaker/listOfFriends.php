<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class ListOfFriends {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function getTheListOfFriends($user_one_name, $user_one_phone, $user_two_name, $user_two_phone, $user_one_fcm_token, $user_two_fcm_token) {

		try {
			
			$select_query = "SELECT id FROM friend_list WHERE (user_one_phone = '$user_one_phone' AND user_two_phone = '$user_two_phone') OR (user_one_phone = '$user_two_phone' AND user_two_phone = '$user_one_phone');";

			$result_query = mysqli_query($this->connection, $select_query);	

			if (mysqli_num_rows($result_query) == 1) {
				$json['error'] = $user_one_name . ' and ' . $user_two_name . ' are friends already';
				echo json_encode($json);
			} else {
				$insert_query = "INSERT INTO friend_list (user_one_name, user_one_phone, user_two_name, user_two_phone, user_one_fcm_token, user_two_fcm_token) VALUES ('$user_one_name', '$user_one_phone', '$user_two_name', '$user_two_phone', '$user_one_fcm_token', '$user_two_fcm_token');";
				$insert_result_query = mysqli_query($this->connection, $insert_query);

				if ($insert_result_query == 1) {
					$json['success'] = 'Added to your friend list!';
				} else {
					$json['error'] = 'Problem adding to friend list! Please try again!';
				}

				echo json_encode($json);
				mysqli_close($this->connection);
			}


		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$listOfFriends = new ListOfFriends();
if(isset($_POST['user_one_name'], $_POST['user_one_phone'], $_POST['user_two_name'], $_POST['user_two_phone'], $_POST['user_one_fcm_token'], $_POST['user_two_fcm_token'])) {

	$user_one_name = $_POST['user_one_name'];
	$user_one_phone = $_POST['user_one_phone'];
	$user_two_name = $_POST['user_two_name'];
	$user_two_phone = $_POST['user_two_phone'];
	$user_one_fcm_token = $_POST['user_one_fcm_token'];
	$user_two_fcm_token = $_POST['user_two_fcm_token'];


	if (!empty($user_one_name) && !empty($user_one_phone) && !empty($user_two_name) && !empty($user_two_phone) && !empty($user_one_fcm_token) && !empty($user_two_fcm_token)) {
		$listOfFriends->getTheListOfFriends($user_one_name, $user_one_phone, $user_two_name, $user_two_phone, $user_one_fcm_token, $user_two_fcm_token);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
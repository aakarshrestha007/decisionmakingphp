<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class User {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function postUserData($username, $phonenumber, $fcm_reg_id) {

		try {
			
			$select_user_query = "SELECT * FROM users WHERE username = '$username' AND phone_number = '$phonenumber';";
			$select_user_result = mysqli_query($this->connection,  $select_user_query);

			if (mysqli_num_rows($select_user_result) > 0) {
				$update_query = "UPDATE users SET fcm_reg_id = '$fcm_reg_id' WHERE username = '$username' AND phone_number = '$phonenumber';";
				$update_result = mysqli_query($this->connection, $update_query);
			} else {
				$insert_query = "INSERT INTO users (username, phone_number, fcm_reg_id) VALUES ('$username', '$phonenumber', '$fcm_reg_id');";
				$insert_result = mysqli_query($this->connection, $insert_query);
				if ($insert_result == 1) {
					$json['success'] = 'Successfully added!';
				} else {
					$json['error'] = 'Problem adding username and phone number!';
				}	
			}

			echo json_encode($json);
			mysqli_close($this->connection);

		} catch (Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
}


$user = new User();
if(isset($_POST['username'], $_POST['phonenumber'], $_POST['fcm_reg_id'])) {

	$username = $_POST['username'];
	$phonenumber = $_POST['phonenumber'];
	$fcm_reg_id = $_POST['fcm_reg_id'];

	if (!empty($username) && !empty($phonenumber) && !empty($fcm_reg_id)) {
		$user->postUserData($username, $phonenumber, $fcm_reg_id);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>
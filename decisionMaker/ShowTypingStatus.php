<?php
include_once 'DBConnection.php';

header('Content-Type: application/json');
	
class ShowTypingStatus {
	
	private $db;
	private $connection;
	
	function __construct() {
		$this -> db = new DB_Connection();
		$this -> connection = $this->db->getConnection();
	}

	public function displayTypingStatus($phonenumber, $fcm_reg_id) {

		/*
		Implementing Cloud Messaging
		*/

		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
				'to' => $fcm_reg_id,
				'notification' => array("title" => "Someone is typing message to you!","body" => $phonenumber . ' is typing!'),
				'data' => array("tyingStatusPhoneNumber" => $phonenumber,
								"typingStatusRecipient_fcm_token" => $fcm_reg_id)
			);

		//Authorization key value is the "Firebase Cloud Messaging token"
		$headers = array(
				'Authorization:key=AAAAvb8pTks:APA91bEkrZg468ZfyK_oEsiYqX5O_tydA0Q8xoude-i29qx5D7Zsc4ESZxotQCTCQOf0zBV9QNErQrE6mEBHxNsSC_uvN1UgRE7juBEDYOMa4I4DPjTXyv9gsmArrQiaB2pmiK-bNXG0CUkWfrZpyPmzTjuEuENjgA',
				'Content-Type: application/json'
			);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

		$result = curl_exec($ch);
		if ($result === FALSE) {
			die('Curl failed: ' . curl_error($ch));
		}
		curl_close($ch);
		
	}
	
}


$showTypingStatus = new ShowTypingStatus();
if(isset($_POST['phonenumber'], $_POST['fcm_reg_id'])) {

	$phonenumber = $_POST['phonenumber'];
	$fcm_reg_id = $_POST['fcm_reg_id'];

	if (!empty($phonenumber) && !empty($fcm_reg_id)) {
		$showTypingStatus->displayTypingStatus($phonenumber, $fcm_reg_id);
	} else {
		$json['error'] = "All fields are required!";
		echo json_encode($json);
	}
}

?>